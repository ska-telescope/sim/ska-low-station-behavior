SKA-Low station behavior simulation
===================================

Introduction
^^^^^^^^^^^^

The StatSim package is a Matlab package to
* simulate the temporal and spectral behavior of the receive paths of an SKA-Low station based on a physics-based model;
* simulate the resulting station beam response;
* simulate intra-station visibilities.
The receive path gain model is described on https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling

Install guide
^^^^^^^^^^^^^

The StatSim package has been organised as a Matlab toolbox, so to use it, just download the StatSim directory and add it to your Matlab path. Typing "help StatSim" should get you started with using the package.

Contact
=======

For questions or comments, please contact Stefan WIjnholds
(wijnholds-at-astron-dot-nl),

.. toctree::
   :maxdepth: 2
   :caption: Contents
