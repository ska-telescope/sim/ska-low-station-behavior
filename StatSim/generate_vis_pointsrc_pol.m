function R = generate_vis_pointsrc_pol(pos, freq, tobs, lon, lat, eep_fun)

% R = generate_vis_pointsrc_pol(pos, freq, tobs, lon, lat, eep_fun)
%
% This function computes the array covariance matrix / visibility matrix
% based on a list of calibration sources for the Southern hemisphere taking
% into account the embedded element patterns (EEPs) of the individual
% antennas. Note: the point source model does not include the Sun.
%
% Arguments
% pos     : Nant-by-2 matrix of (x, y)-positions in the horizon plane of
%           the station in m
% freq    : observing frequency in Hz
% tobs    : observing time as datenum in UTC
% lon     : geographical longitude of the station site in rad
% lat     : geographical latitude of the station site in rad
% eep_fun : function handle to a function to evaluate the embedded element
%           patterns at specified (l, m)-coordinates in terms of their
%           Jones matrices
%
% Return value
% R       : (2 * Nant)-by-(2 * Nant) array covariance matrix in which
%           each 2-by-2 block represents the coherence / visibility of a
%           single intra-station baseline
%
% Stefan J. Wijnholds, 17 August 2020

% useful constant
Nant = size(pos, 1);

%% Source model

% load list of calibration sources
load SHsource_data
rasrc = SHsources.ra;
decsrc = SHsources.dec;

% determine where the sources are in the local horizon system
[lsrc, msrc] = radectolm(rasrc, decsrc, JulianDay(tobs), lon * 180 / pi, lat * 180 / pi);
% if position is NaN, the source is below local horizon plane
srcsel = ~isnan(lsrc);
lm = [lsrc(srcsel), msrc(srcsel)];
% estimate flux at observing frequency assuming exponential power spectrum
flux70MHz = SHsources.flux70MHz;
flux450MHz = SHsources.flux450MHz;
flux70MHz = flux70MHz(srcsel);
flux450MHz = flux450MHz(srcsel);
Nsrc = length(flux70MHz);
logflux = zeros(Nsrc, 1);
for idx = 1:Nsrc
    logflux(idx) = interp1([70e6, 450e6], log10([flux70MHz(idx), flux450MHz(idx)]), freq, 'linear');
end
sigma_src = 10.^logflux;

%% Instrument model
J = eep_fun(lm(:, 1), lm(:, 2));

%% Array covariance matrix
R = zeros(2 * Nant, 2 * Nant);
for srcidx = 1:Nsrc
    % array response matrix including element response
    % note that delay has been included in EEPs
    A = zeros(2 * Nant, 2);
    for antidx = 1:Nant
        A(2 * antidx - 1:2 * antidx, :) = J(:, :, antidx, srcidx);
    end
    % add source to visibilities
    R = R + sigma_src(srcidx) * (A * A');
end
