function [gFOx, gFOy] = gen_gFO(f, epsmag_std, epsph_std, L0FO, TFO)

% [gFOx, gFOy] = gen_gFO(f, epsmag_std, epsph_std, TFO)
%
% Function to generate gains of the RFoF link of the SKA-LOW antennas
% based on the physical model:
%
% gFO = g0 * exp(1i * phi_FO)
%
% where the phase of the RFoF link is calculated by
%
% phi_FO = 2 * pi * f * (n * alpha + beta) * (L0FO * (TFO - 293))/ c
%
% For more details on this model and the underlying assumptions, the reader
% is referred to
% https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling
%
% Arguments:
% f          : Nf-element column vector with frequencies at which to
%              generate gain values (in Hz)
% epsmag_std : RMS gain magnitude variation between receive paths (in dB)
% epsph_std  : RMS gain phase variations between receive paths (in deg)
% L0FO       : Nant-element column vector with nominal fibre optic cable
%              lengths (in m)
% TFO        : Nant x Nt matrix with temperature values for each RFoF link
%              and time slot (in K)
%
% Return value
% gFOx       : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for x-polarisation
% gFOx       : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for y-polarisation
%
% Stefan J. Wijnholds, 3 August 2020
% Last modified on 17 August 2020 by Stefan J. Wijnholds

% determine simulation size
Nf = length(f);
Nt = size(TFO, 2);
Nant = size(TFO, 1);

% properties of fiber optic cable
n = 1.47;           % refractive index
alpha = 5.6e-7;     % linear thermal expansion coefficient of fiber (1/K)
beta = 1.2e-5;      % thermal coefficient of the refractive index (1/K)
T0 = 293;           % reference temperature (K)
c = 2.99792e8;      % speed of light in vacuum (m/s)

% compute phase of the RFoF link
phi_FO = zeros(Nant, Nf, Nt);
for antidx = 1:Nant
    phi_FO(antidx, :, :) = 2 * pi * f * (n * alpha + beta) * L0FO(antidx) * (TFO(antidx, :) - T0) / c;
end

% production tolerances
% modelled as constant offsets across frequency for each element, offsets
% assumed to have Gaussian distribution across elements
epsmag = epsmag_std * randn(2 * Nant, 1);
epsph = epsph_std * randn(2 * Nant, 1);
gerr = 10.^(epsmag/20) .* exp(1i * epsph * pi / 180);

% RFoF gain at desired frequencies and time slices
gFOx = zeros(Nant, Nf, Nt);
gFOy = zeros(Nant, Nf, Nt);
for antidx = 1:Nant
    gFOx(antidx, :, :) = gerr(antidx) * exp(1i * phi_FO(antidx, :, :));
    gFOy(antidx, :, :) = gerr(Nant + antidx) * exp(1i * phi_FO(antidx, :, :));
end
