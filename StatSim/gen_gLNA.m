function [gLNAx, gLNAy] = gen_gLNA(f, epsmag_std, epsph_std, TLNA)

% [gLNAx, gLNAy] = gen_gLNA(f, epsmag_std, epsph_std, TLNA)
%
% Function to generate gains of the LNA module of the SKA-LOW antennas
% based on the physical model:
%
% gLNA = g0 * 10^(-0.014 * (TLNA - Tref) / 20)
%
% For more details on this model and the underlying assumptions, the reader
% is referred to
% https://confluence.skatelescope.org/display/TDT/Signal+Chain+Behavioural+Modeling
%
% Arguments:
% f          : Nf-element column vector with frequencies at which to
%              generate gain values (in Hz)
% epsmag_std : RMS gain magnitude variation between receive paths (in dB)
% epsph_std  : RMS gain phase variations between receive paths (in deg)
% TLNA0      : Nant x Nt matrix with LNA temperatures for each LNA and
%              time instance (in K)
%
% Return value
% gLNAx      : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for x-polarisation
% gLNAy      : Nant x Nf x Nt tensor containing generated LNA gain values
%              (complex valued, voltage domain) for y-polarisation
%
% Stefan J. Wijnholds, 29 July 2020
% Last modified on 17 August 2020 by Stefan J. Wijnholds

% determine simulation size
Nf = length(f);
Nt = size(TLNA, 2);
Nant = size(TLNA, 1);

% fiducial frequencies (Hz)
freq = (50:40:330).' * 1e6;
% reference temperature (K)
Tref = 263;

% gain magnitude at Tref at fiducial frequencies (dB), read from graph
% note: ripple due to impedance mismatch ignored as possibility to put a
% matching circuit in place is expected to be used in the future
gmag = [43.6 45.2 46.1 46.5 46.6 46.4 46.0 45.4].';
% gain phase at 263 K at fiducial frequencies (deg), read from graph
gph = [37 -4 -32 -58 -84 -107 -131 -153].';
% magnitude sensitivity to temperature (dB/K)
dgmag = -0.014;
% phase sensitivity to temperature (deg/K)
% dgph = -0.1; (not used, can optionally be added later)

% production tolerances
% modelled as constant offsets across frequency for each element, offsets
% assumed to have Gaussian distribution across elements
epsmag = epsmag_std * randn(2 * Nant, 1);
epsph = epsph_std * randn(2 * Nant, 1);

% LNA gain at desired frequencies and time slices
gLNAx = zeros(Nant, Nf, Nt);
gLNAy = zeros(Nant, Nf, Nt);
g0mag = interp1(freq, gmag, f);
g0ph = interp1(freq, gph, f);
for elemidx = 1:Nant
    g0x = 10.^((g0mag + epsmag(elemidx)) / 20) .* exp(1i * (g0ph + epsph(elemidx)) * pi / 180);
    gLNAx(elemidx, :, :) = g0x * 10.^(dgmag * (TLNA(elemidx, :) - Tref) / 20);
    g0y = 10.^((g0mag + epsmag(Nant + elemidx)) / 20) .* exp(1i * (g0ph + epsph(Nant + elemidx)) * pi / 180);
    gLNAy(elemidx, :, :) = g0y * 10.^(dgmag * (TLNA(elemidx, :) - Tref) / 20);
end

