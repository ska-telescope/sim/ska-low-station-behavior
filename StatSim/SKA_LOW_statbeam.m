function Jbeam = SKA_LOW_statbeam(l0, m0, freq, g, l, m)

% Jbeam = SKA_LOW_statbeam(l0, m0, freq, g, l, m)
%
% This function predicts the SKA-LOW station response in terms of Jones
% matrices towards the specified directions while the station beam is
% pointed towards (l0, m0). The receive path gains are taken into account.
%
% Arguments
% l0    : l-coordinate of pointing direction
% m0    : m-coordinate of pointing direction
% freq  : operating frequency of the station in Hz
% g     : (2 * Nant)-element vector with receive path gains assumed to be
%         ordered as [g_{1,x}, g_{1,y}, ..., g_{Nant,x}, g_{Nant,y}]
% l     : Ndir-element vector with l-coordinates of directions of interest
% m     : Ndir-element vector with m-coordinates of directions of interest
%
% Return value
% Jbeam : 2-by-2-by-Ndir tensor describing the station response
%
% Stefan J. Wijnholds, 10 August 2020

% load array configuration
load Arraylayout_DiNinni_SKA-LOW_38m.mat Arraylayout
xpos = Arraylayout(:, 1);
ypos = Arraylayout(:, 2);
c = 2.99792e8;  % speed of light (m/s)

% calculate beamformer weights
w = exp((-2 * pi * 1i * freq / c) * (xpos * l0 + ypos * m0));
% augment to two polarisations
w = kron(w, [1; 1]);

% determine element responses in specified directions
Jelem = pol_EEP_SKALA4AL(l, m);

% determine station response
Ndir = length(l);
Nant = size(Jelem, 3);
Jweighted = zeros(2, 2, Ndir, Nant);
for diridx = 1:Ndir
    for antidx = 1:Nant
        Jweighted(:, :, diridx, antidx) = diag(w(2*antidx-1:2*antidx)) * diag(g(2*antidx-1:2*antidx)) * squeeze(Jelem(:, :, antidx, diridx));
    end
end
Jbeam = sum(Jweighted, 4);
