function J = pol_EEP_SKALA4AL(l, m)

% J = pol_EEP_SKALA4AL(l, m)
%
% This function returns the polarized element response as Jones matrix of
% 256 SKALA4AL antennas in a proposed SKA-low configuration.
%
% Note: at the moment, the patterns at 110 MHz are returned. A discussion
% is needed about whether interpolation between the simulated spot
% frequencies is possible and, if so, how that should be done.
%
% Arguments
% l : Ndir-element vector with l-coordinates of directions of interest
% m : Ndir-element vector with m-coordinates of directions of interest
%
% Return value
% J : 2-by-2-by-256-Ndir tensor containing 2-by-2 Jones matrices for each
%     of the 256 elements for the specified directions
%
% Stefan J. Wijnholds, 10 August 2020

% load SKALA4AL EEPs
load /Users/wijnholds/data-reduction/LFAA_study_EEP_differences/SKALA4AL_data/EEP_SKALA4AL_110MHz_38m.mat Ephi Eth theta phi

% define grid used during simulations
Nt = length(theta);
Np = length(phi);
Nant = 256;
freq = 110e6;   % frequency of EM simulation in Hz
c = 2.99792e8;  % speed of light in m/s
% convert to radian
theta = theta * pi / 180;
phi = phi * pi / 180;
% augment vector of phi to avoid interpolation issues
phi = [2 * phi(1) - phi(2), phi, 2 * phi(end) - phi(end-1)];

% The antennas share a common phase reference, resulting in steep phase
% gradiens in the EEPs for elements further away from the phase reference
% position. This may cause interpolation issues, so we load the array
% configuration to compensate this geometrical delay
load Arraylayout_DiNinni_SKA-LOW_38m.mat Arraylayout
% scale to assumed 38-m layout
xpos = Arraylayout(:, 1);
ypos = Arraylayout(:, 2);
% define grid for delay correction
[phigrid, thetagrid] = meshgrid(phi, theta);

% determine gains for specified directions
Ndir = length(l);
J = zeros(2, 2, 256, Ndir);
lmdist = sqrt(l.^2 + m.^2);
% convert (l,m) to (theta, phi)
phi0 = atan2(l(lmdist <= 1), m(lmdist <= 1)) + pi;
theta0 = asin(lmdist(lmdist <= 1));
% pre-allocation
Eph = zeros(Nt, Np+2);
Etheta = zeros(Nt, Np+2);
for antidx = 1:Nant
    J(:, :, antidx, lmdist > 1) = NaN;
    % calculate delay correction for (phi, theta)-grid
    delaycor_thphi = exp((-2 * pi * 1i * freq / c) * (xpos(antidx) * cos(phigrid) .* sin(thetagrid) + ypos(antidx) * sin(phigrid) .* sin(thetagrid)));
    % calculate delay correction for (l, m)-grid
    delaycor_lm = exp((-2 * pi * 1i * freq / c) * (xpos(antidx) * l(lmdist <= 1) + ypos(antidx) * m(lmdist <= 1)));
    for pol = 1:2
        % collect data on Ephi and Etheta
        Eph(:, 2:end-1) = reshape(Ephi(1, pol, antidx, :), [Nt, Np]);
        Eph(:, 1) = Eph(:, end-1);
        Eph(:, end) = Eph(:, 2);
        Etheta(:, 2:end-1) = reshape(Eth(1, pol, antidx, :), [Nt, Np]);
        Etheta(:, 1) = Etheta(:, end-1);
        Etheta(:, end) = Etheta(:, 2);
        % The following lines perform three operations
        % 1. move the phase reference to the element being considered;
        % 2. regridding from (theta, phi)-grid to (l, m)-grid by
        % 3. move the phase reference back to common phase reference
        J(pol, 1, antidx, lmdist <= 1) = interp2(phi, theta, Eph .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
        J(pol, 2, antidx, lmdist <= 1) = interp2(phi, theta, Etheta .* delaycor_thphi, phi0, theta0) .* conj(delaycor_lm);
    end
end




