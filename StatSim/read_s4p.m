function [freq, sparams] = read_s4p(filename)

% [freq, sparams] = read_s4p(filename)
%
% Function to read s-parameters from an s4p file.
%
% Argument
% filename  : string containing name of measurement data file
%
% Return values
% freq      : Nf-element vector containing measurement frequencies in Hz
% sparams   : Nf-by-32 matrix containing magnitudes and phases of the
%             s-parameters of a 4-port network (s11, s12, s13, s14, s21,
%             s22, s23, s24, s31, s32, s33, s34, s41, s42, s43, s44)
%
% Note: this function is the result from a reverse engineering effort to
% extract s-parameter data from SKA-LOW component measurements and is
% therefore not intended for general usage.
%
% Stefan J. Wijnholds, 19 January 2021

% open file read-only
fid = fopen(filename, 'r');

% skip meta-data
line = fgetl(fid);
while (line(1) == '!')
    line = fgetl(fid);
end
% Note: the last line read contains the header information of the data
% segment. The next read command will start reading the data segment.

% read data
freq = [];
sparams = [];
% Read data associated with first frequency
[data, count] = fscanf(fid, '%f', 33);
while count
    freq = [freq; data(1)];
    sparams = [sparams; data(2:end).'];
    % attempt reading data for next frequency
    [data, count] = fscanf(fid, '%f', 33);
end

fclose(fid);
