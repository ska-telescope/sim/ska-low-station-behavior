function dyn_scatter_plot(pos, data, stepsize, label)

% dyn_scatter_plot(pos, data, stepsize, label)
%
% Function to produce dynamic scatter plots of time series associated with
% the antennas located at (xpos, ypos).
%
% Arguments
% pos       : N-by-2 matrix with (x, y)-coordinates
% data      : N-by-Nt matrix with data to plot
% stepsize  : stepsize along the time axis
% label     : string containing text for plot title
%
% Stefan J. Wijnholds, 3 August 2020

% determine number of timeslots
Nt = size(data, 2);
% show data
for tidx = 1:stepsize:Nt
    scatter(pos(:, 1), pos(:, 2), 30, data(:, tidx), 'filled');
    set(gca, 'FontSize', 16);
    xlabel('x (m)');
    ylabel('y (m)');
    set(colorbar, 'FontSize', 16);
    title([label ' (' num2str(tidx) ' of ' num2str(Nt) ')']);
    pause(0.1);
end
