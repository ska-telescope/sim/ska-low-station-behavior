%% Analyse component level measurements of FEMs for AAVS2
%
% This script aims to establish production tolerances for the FEMs for use
% in the SKA-LOW receive path gain model based on measurements performed by
% the INAF team (lead: Jader Monari and Federico Perini). The INAF team
% have already summarised the measurement results in a .mat file.
% Unfortunately, that summary only contains the gain magnitude of each
% receive path and not its phase. This script therefore uses the raw
% measurement data, for which an ad hoc function was written to read the
% s-parameter files. This script validates that function by comparing the
% gain magnitude data extracted with the gain magnitude data collected by
% the INAF team.
%
% As the intercomponent variatiosn vary significantly with frequency, the
% calculated tolerances are stored in a .mat file that can be read in by
% the gen_gFEM function.
%
% Stefan J. Wijnholds, 20 January 2021
% Last modified on 25 January 2021 by Stefan J. Wijnholds

%% start with a clean workspace
clear
close all

%% Read and validate data

% The data reside on a Google Drive and can be made available up request.
raw_data_dir = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/280 FEM AAVS2/RAW Measurements/';
summary_file = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/280 FEM AAVS2/Measure_AAVS22.mat';
meas_freq_file = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/Frequency.mat';
load(summary_file);
load(meas_freq_file);
NFEM = 280;
Nf = length(Frequency_S);
g1 = zeros(NFEM, Nf);
g2 = zeros(NFEM, Nf);
for FEMidx = 1:NFEM
    datafile = [raw_data_dir 'S_' num2str(FEMidx, '%03d') '.s4p'];
    [~, sparams] = read_s4p(datafile);
    % calculate complex gain of first receive path based on s21
    g1(FEMidx, :) = 10.^(sparams(:, 9) / 20) .* exp(1i * sparams(:, 10) * pi / 180);
    % calculate complex gain of second receive path based on s43
    g2(FEMidx, :) = 10.^(sparams(:, 29) / 20) .* exp(1i * sparams(:, 30) * pi / 180);
end

% show difference between summary INAF team and raw data
figure
plot(Frequency_S, Misure.Gain_RF1 - 20 * log10(abs(g1)).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain first receive path (dB)');
title('difference, issue with no. 53');

figure
plot(Frequency_S, Misure.Gain_RF2 - 20 * log10(abs(g2)).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain second receive path (dB)');
title('difference, issue with no. 53');

% due to issue with no. 53, we deselect it for further analysis
FEMsel = [1:52 54:280];

%% analyse component variations

% focus on SKA-LOW operating frequency range
fsel = (Frequency_S >= 50) & (Frequency_S <= 350);

% start with first receive path of each FEM
% plot gain magnitude and phase to see variations
figure
plot(Frequency_S(fsel), 20 * log10(abs(g1(FEMsel, fsel))).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain magnitude of first receive path');

figure
plot(Frequency_S(fsel), unwrap(angle(g1(FEMsel, fsel)).') * 180 / pi);
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain phase of first receive path');

% calculate statistics
std_mag1 = std(20 * log10(abs(g1(FEMsel, fsel))));
std_ph1 = std(unwrap(angle(g1(FEMsel, fsel)))) * 180 / pi;

% show result
figure
[ax, ~, ~] = plotyy(Frequency_S(fsel), std_mag1, Frequency_S(fsel), std_ph1);
set(ax(1), 'FontSize', 16);
set(ax(2), 'FontSize', 16);
xlabel('frequency (MHz)');
title('variations first receive path');
ylabel(ax(1), 'gain magnitude variation (dB)');
ylabel(ax(2), 'gain phase variation (deg)');

% repeat for second receive path of each FEM
% plot gain magnitude and phase to see variations
figure
plot(Frequency_S(fsel), 20 * log10(abs(g2(FEMsel, fsel))).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain magnitude of second receive path');

figure
plot(Frequency_S(fsel), unwrap(angle(g2(FEMsel, fsel)).') * 180 / pi);
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain phase of second receive path');

% calculate statistics
std_mag2 = std(20 * log10(abs(g2(FEMsel, fsel))));
std_ph2 = std(unwrap(angle(g2(FEMsel, fsel)))) * 180 / pi;

% show result
figure
[ax, ~, ~] = plotyy(Frequency_S(fsel), std_mag2, Frequency_S(fsel), std_ph2);
set(ax(1), 'FontSize', 16);
set(ax(2), 'FontSize', 16);
xlabel('frequency (MHz)');
title('variations second receive path');
ylabel(ax(1), 'gain magnitude variation (dB)');
ylabel(ax(2), 'gain phase variation (deg)');

%% store result
fmeas = Frequency_S(fsel) * 1e6; % store measurement frequencies in Hz
save tolerances_FEM.mat fmeas std_mag1 std_ph1 std_mag2 std_ph2
