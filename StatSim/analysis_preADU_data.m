%% Analyse component level measurements of preADUs for EDA2
%
% This script aims to establish production tolerances for the preADUs for
% use in the SKA-LOW receive path gain model based on measurements
% performed by the INAF team (lead: Jader Monari and Federico Perini). The
% INAF team have already summarised the measurement results in a .mat file.
% Unfortunately, that summary only contains the gain magnitude of each
% receive path and not its phase. This script therefore uses the raw
% measurement data, for which an ad hoc function was written to read the
% s-parameter files. This script validates that function by comparing the
% gain magnitude data extracted with the gain magnitude data collected by
% the INAF team.
%
% As the intercomponent variatiosn vary significantly with frequency, the
% calculated tolerances are stored in a .mat file that can be read in by
% the gen_g_preADU function.
%
% Stefan J. Wijnholds, 20 January 2021
% Last modified on 25 January 2021 by Stefan J. Wijnholds

%% start with a clean workspace
clear
close all

%% Read and validate data

% The data reside on a Google Drive and can be made available up request.
raw_data_dir = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/18 PreADU EDA2/RAW Measurements/';
summary_file = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/18 PreADU EDA2/Measure_18PreADU_EDA2.mat';
meas_freq_file = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data component tolerances/Frequency.mat';
load(summary_file);
load(meas_freq_file);
Nch = 144;  % number of dual-pol channels
Nf = length(Frequency_S);
g1 = zeros(Nch, Nf);
g2 = zeros(Nch, Nf);
for chidx = 1:Nch
    datafile = [raw_data_dir 'S_' num2str(chidx, '%03d') '.s4p'];
    [~, sparams] = read_s4p(datafile);
    % calculate complex gain of first receive path based on s21
    g1(chidx, :) = 10.^(sparams(:, 9) / 20) .* exp(1i * sparams(:, 10) * pi / 180);
    % calculate complex gain of second receive path based on s43
    g2(chidx, :) = 10.^(sparams(:, 29) / 20) .* exp(1i * sparams(:, 30) * pi / 180);
end

% show difference between summary INAF team and raw data
figure
plot(Frequency_S, Misure.Gain_RF1 - 20 * log10(abs(g1)).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain (dB)');
title('difference for first receive path');

figure
plot(Frequency_S, Misure.Gain_RF2 - 20 * log10(abs(g2)).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain (dB)');
title('difference for second receive path');

%% analyse component variations

% focus on SKA-LOW operating frequency range
fsel = (Frequency_S >= 50) & (Frequency_S <= 350);

% start with first receive path of each FEM
% plot gain magnitude and phase to see variations
figure
plot(Frequency_S(fsel), 20 * log10(abs(g1(:, fsel))).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain magnitude of first receive path');

figure
plot(Frequency_S(fsel), unwrap(angle(g1(:, fsel)).') * 180 / pi);
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain phase of first receive path');

% calculate statistics
std_mag1 = std(20 * log10(abs(g1(:, fsel))));
std_ph1 = std(unwrap(angle(g1(:, fsel)))) * 180 / pi;

% show result
figure
[ax, ~, ~] = plotyy(Frequency_S(fsel), std_mag1, Frequency_S(fsel), std_ph1);
set(ax(1), 'FontSize', 16);
set(ax(2), 'FontSize', 16);
xlabel('frequency (MHz)');
title('variations first receive path');
ylabel(ax(1), 'gain magnitude variation (dB)');
ylabel(ax(2), 'gain phase variation (deg)');

% repeat for second receive path of each FEM
% plot gain magnitude and phase to see variations
figure
plot(Frequency_S(fsel), 20 * log10(abs(g2(:, fsel))).');
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain magnitude of second receive path');

figure
plot(Frequency_S(fsel), unwrap(angle(g2(:, fsel)).') * 180 / pi);
set(gca, 'FontSize', 16);
xlabel('frequency (MHz)');
ylabel('gain phase of second receive path');

% calculate statistics
std_mag2 = std(20 * log10(abs(g2(:, fsel))));
std_ph2 = std(unwrap(angle(g2(:, fsel)))) * 180 / pi;

% show result
figure
[ax, ~, ~] = plotyy(Frequency_S(fsel), std_mag2, Frequency_S(fsel), std_ph2);
set(ax(1), 'FontSize', 16);
set(ax(2), 'FontSize', 16);
xlabel('frequency (MHz)');
title('variations second receive path');
ylabel(ax(1), 'gain magnitude variation (dB)');
ylabel(ax(2), 'gain phase variation (deg)');

%% store result
fmeas = Frequency_S(fsel) * 1e6; % store measurement frequencies in Hz
save tolerances_preADU.mat fmeas std_mag1 std_ph1 std_mag2 std_ph2

