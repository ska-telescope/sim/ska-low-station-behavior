function AF = calculate_AF(x, y, gain, l, m, freq, l0, m0)

% AF = calculate_AF(x, y, gain, l, m, freq, l0, m0)
%
% Function to calculate the array factor of a planar array taking receive
% path gain errors into account.
%
% Arguments
% x     : Nelem-by-1 vector with x-positions of the antennas (in m)
% y     : Nelem-by-1 vector with y-positions of the antennas (in m)
% gain  : Nelem-by-Nelem diagonal matrix with complex valued receive path
%         gains on the main diagonal
% l     : Nl-by-1 vector of grid values in l-direction
% m     : Nm-by-1 vector of grid values in m-direction
% freq  : frequency (in Hz)
% l0    : Ndir-by-1 vector of l-coordinates of pointing directions
% m0    : Ndir-by-1 vector of m-coordinates of pointing directions
%
% Return value
% AF    : Nl-by-Nm-by Ndir tensor describing the array factor in the
%         voltage domain for Ndir pointing direction on an Nl-by-Nm grid
%
% Stefan J. Wijnholds, 1 August 2007
% Renamed by Stefan J. Wijnholds on 26 April 2021
% Code cleaned up by Stefan J. Wijnholds on 11 May 2021

c = 2.99792e8;      % speed of light (in m/s)
lambda = c / freq;  % wavelength (in m)

xsrcdelay = -x(:) * l0(:).';
ysrcdelay = -y(:) * m0(:).';
arrayvec = gain * exp(-2 * pi * 1i * (xsrcdelay + ysrcdelay) / lambda);
norm = sqrt(sum(abs(arrayvec).^2));
Wx = exp(-2 * pi * 1i * l(:) * x(:).' / lambda);
Wy = exp(-2 * pi * 1i * m(:) * y(:).' / lambda);
W = khatrirao(Wx, Wy);
AF = W * arrayvec * diag(1 ./ norm);
AF = reshape(AF, [length(l), length(m), length(l0)]);
