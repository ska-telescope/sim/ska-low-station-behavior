%% Retrieve data from temperature loggers and MRO weather station
%
% The aim of this script is to read data from selected temperature loggers
% installed in EDA2 and AAVS2 along with data from the MRO weather station
% covering the same time span. The script provides an inspection plot that
% facilitates making a selection of the temperature data that can be stored
% in a .mat file as input for the recieve path gain model.
%
% Stefan J. Wijnholds, 8 February 2021

%% start with a clean workspace
clear
close all

%% location of data files
prefix = '~/SKA/SKA-low bridging/SKA-low calibration/receive path gain model/Data temperature logging/Temperature Logging/Data Sets/';
data_logger{1} = '3/Pro-1/Pro1_EDA2_tile7_SB17_out 20191210.txt';
data_logger{2} = '3/Pro-2/Pro2_EDA2_tile6_SB6_TR_out 20191210.txt';
data_logger{3} = '3/Pro-3/Pro3_AAVS2_tile7_SB22_out 20191210.txt';
data_logger{4} = '3/Pro-4/Pro4_AAVS2_tile6_SB21_TR_out 20191210.txt';
data_weather_stat = 'MRO Weather Archive_OzForecast/MRO Weather Archive_Downloaded 20200331.csv';

%% read data from loggers
Nlogger = length(data_logger);
t = cell(Nlogger, 1);
Temp = cell(Nlogger, 1);
for idx = 1:4
    disp(['reading data from logger ' num2str(idx) ' of ' num2str(Nlogger)]);
    fid = fopen([prefix data_logger{idx}], 'r');
    % skip first line
    fgetl(fid);
    dataidx = 1;
    time_data = cell(1);
    Temp_data = cell(1);
    while ~isempty(fscanf(fid, '%d', 1))
        fscanf(fid, '%c', 1);
        time_data{dataidx} = fscanf(fid, '%c', 19);
        fscanf(fid, '%c', 1);
        Temp_data{dataidx} = fscanf(fid, '%f', 1);
        fgetl(fid);
        dataidx = dataidx + 1;
    end
    t{idx} = datenum(time_data, 'yyyy-mm-dd hh:MM:ss');
    Temp{idx} = [Temp_data{:}];
    fclose(fid);
end

%% read data from MRO weather station
fid = fopen([prefix, data_weather_stat], 'r');
% skip header
fgetl(fid);
fgetl(fid);
fgetl(fid);
fgetl(fid);
dataidx = 1;
time_data = cell(1);
Temp_data = cell(1);
while ~feof(fid)
    time_data{dataidx} = fscanf(fid, '%c', 19);
    fscanf(fid, '%c', 1);
    Temp_data{dataidx} = fscanf(fid, '%f', 1);
    fgetl(fid);
    dataidx = dataidx + 1;
end
fclose(fid);
tMRO = datenum(time_data, 'yyyy-mm-dd hh:MM:ss');
TempMRO = [Temp_data{:}];
% select relevant part of the data
tstart = min([min(t{1}), min(t{2}), min(t{3}), min(t{4})]);
tstop = max([max(t{1}), max(t{2}), max(t{3}), max(t{4})]);
sel = (tMRO >= tstart) & (tMRO <= tstop);
tMRO = tMRO(sel);
TempMRO = TempMRO(sel);

%% show data
figure
plot(t{1}, Temp{1}, '-', ...
     t{2}, Temp{2}, '-', ...
     t{3}, Temp{3}, '-', ...
     t{4}, Temp{4}, '-', ...
     tMRO, TempMRO, '.');
set(gca, 'FontSize', 16);
xlabel('time (datenum)');
ylabel('Temperature (degrees C)');
legend('EDA2', 'EDA2 with roof', 'AAVS2', 'AAVS2 with roof', 'MRO station', 'Location', 'SouthEast');
 
%% show response of SmartBoxes to outside temperature
diff = cell(1);
for idx = 1:Nlogger
    TempMROint = interp1(tMRO, TempMRO, t{idx});
    diff{idx} = Temp{idx} - TempMROint.';
end
figure
plot(t{1}, diff{1}, '-', ...
     t{2}, diff{2}, '-', ...
     t{3}, diff{3}, '-', ...
     t{4}, diff{4}, '-');
set(gca, 'FontSize', 16);
xlabel('time (datenum)');
ylabel('Temperature difference (degrees C)');
legend('EDA2', 'EDA2 with roof', 'AAVS2', 'AAVS2 with roof', 'Location', 'SouthEast');

%% select data
sel = 1:24 * 60;    % data is sampled with 1-minute cadence, so this
                    % selects one day
timestamp = t{3}(sel);
Tamb = interp1(tMRO, TempMRO, t{3}(sel));
TSB = (Temp{3}(sel)).';
save tempdata.mat timestamp Tamb TSB

