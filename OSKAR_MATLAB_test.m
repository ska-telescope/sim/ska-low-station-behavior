% This script was written for DP ART System Demo 8.5.
%
% It demonstrates that data stored by OSKAR in MS format and converted to a
% .mat file by ms_to_matlab.py can be read into Matlab and imaged to get a
% similar imaging result as by imaging the MS in OSKAR directly. This
% validates the various conversions involved in reading visibilities
% simulated by OSKAR into Matlab.
%
% Stefan J. Wijnholds, 26 October 2020
% last updated by Stefan J. Wijnholds on 10 November 2020

%% start with a clean workspace
clear
close all

%% input data and meta-data

% directory in which inputndata resides
datadir = '/Users/wijnholds/SKA/SKA-low bridging/SKA-low calibration/OSKAR/';

posfile = 'single_station.tm/layout.txt';   % antenna positions
visdata = 'snapshot_test.mat';              % converted visibility data
imdata = 'snapshot_test.fits';              % image file from OSKAR
freq = 100e6;                               % simulation frequency (Hz)
lm = -1:0.02:1;                             % lm-grid for imaging

%% read antenna positions
fid = fopen([datadir posfile], 'r');
pos = fscanf(fid, '%f,%f');
xpos = pos(1:2:end);
ypos = pos(2:2:end);
fclose(fid);

%% read and image visibility data
load([datadir visdata]);
sel = triu(ones(256), 1);
% read xx-correlations
R = zeros(256);
R(sel.' == 1) = vis(:, 1, 1);
Rxx = R + R';
% read yy-correlations
R = zeros(256);
R(sel.' == 1) = vis(:, 1, 4);
Ryy = R + R';
% imaging
Ixx = acm2skyimage(conj(Rxx), xpos, ypos, freq, lm, lm);
Iyy = acm2skyimage(conj(Ryy), xpos, ypos, freq, lm, lm);
Itot = Ixx + Iyy;
% define mask to remove unphysical area
lmdist = sqrt(meshgrid(lm).^2 + meshgrid(lm).'.^2);
mask = ones(length(lm));
mask(lmdist > 1) = NaN;
% show result
figure;
% transpose to flip l and m axis to first coordinate on horizontal axis
imagesc(lm, lm, (Itot .* mask).');
set(gca, 'FontSize', 16, 'YDir', 'normal');
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m \rightarrow North');
title('Image made in Matlab');

%% read and show OSKAR image
data = fitsread([datadir imdata]);
Np = size(data, 1);
figure;
% flip l-axis to compensate opposite direcitons of l- and x-axes in FITS
imagesc(-1+0.5/Np:1/Np:1-0.5/Np, -1+0.5/Np:1/Np:1-0.5/Np, fliplr(data));
set(gca, 'FontSize', 16, 'YDir', 'normal');
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m \rightarrow North');
title('Image made in OSKAR');