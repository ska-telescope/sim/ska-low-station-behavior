#!/usr/bin/env python3
import argparse

import numpy
import oskar
from scipy.io import savemat

def main():
    parser = argparse.ArgumentParser(
        description='Export visibilities and (u,v,w) coordinates from a '
                    'Measurement Set to a MATLAB file',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('ms_name', help='Measurement Set path')
    parser.add_argument('-t', type=int, default=0, help='Time index to read')
    parser.add_argument('--out', default='out.mat',
                        help='Output MATLAB file name')
    args = parser.parse_args()

    # Open the Measurement Set and get the number of instantaneous baselines.
    ms = oskar.MeasurementSet.open(args.ms_name)
    num_stations = ms.num_stations
    num_baselines = num_stations * (num_stations - 1) // 2

    # Read data for a specific time index.
    time_index = args.t
    start_row = time_index * num_baselines
    (u, v, w) = ms.read_coords(start_row, num_baselines)
    vis = ms.read_column('DATA', start_row, num_baselines)

    # Write as a MATLAB file.
    uvw = numpy.column_stack((u, v, w))
    savemat(args.out, {'uvw': uvw, 'vis': vis})


if __name__ == '__main__':
    main()
