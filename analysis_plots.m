%% Script to produce analysis plots of receive path gain model
%
% Please run the script receive_path_gain first
%
% Stefan J. Wijnholds, 20 August 2020
% Last modified by Stefan J. Wijnholds on 24 August 2020

%% close all figures
close all

%% show ambient temperature and solar irradiation
figure
[ax, ~, ~] = plotyy(t / 3600, T0, t/ 3600, L0);
set(ax(1), 'FontSize', 16, 'Position', [0.13 0.13 0.7250 0.8150]);
set(ax(2), 'FontSize', 16, 'Position', [0.13 0.13 0.7250 0.8150]);
xlabel('time (hours)');
ylabel(ax(1), 'ambient temperature (K)');
ylabel(ax(2), 'solar irradiation (W / m^2)');

%% show example of temperature variations between components
figure
plot(t / 3600, TFEM.');
set(gca, 'FontSize', 16);
xlabel('time (hours)');
ylabel('temperature (K)');
title('temperature variation between FEMs');

%% total gain phase variation during a day
figure
fidx = 13;
plot(t / 3600, unwrap(angle(squeeze(g_totx(:, fidx, :))), [], 2) * 180 / pi);
set(gca, 'FontSize', 16)
xlabel('time (hours)');
ylabel('phase (deg)');
title(['gain phase variations at ' num2str(f(fidx) / 1e6) ' MHz']);

%% RMS gain phase variations between receive paths
figure
plot(t / 3600, std(unwrap(angle(squeeze(g_totx(:, fidx, :))))) * 180 / pi, 'k-', ...
     t / 3600, std(unwrap(angle(squeeze(gLNAx(:, fidx, :))))) * 180 / pi, 'b--', ...
     t / 3600, std(unwrap(angle(squeeze(gFEMx(:, fidx, :))))) * 180 / pi, 'm--', ...
     t / 3600, std(unwrap(angle(squeeze(gFOx(:, fidx, :))))) * 180 / pi, 'r--', ...
     t / 3600, std(unwrap(angle(squeeze(g_preADUx(:, fidx, :))))) * 180 / pi, 'g--');
set(gca, 'FontSize', 16);
xlabel('time (hours)');
ylabel('RMS phase (deg)');
title(['RMS gain phase at ' num2str(f(fidx) / 1e6) ' MHz']);
legend('total', 'LNA', 'FEM', 'FO', 'pre-ADU', 'Location', 'SouthEast');

%% RMS gain magnitude variations between receive paths
figure
plot(t / 3600, std(20 * log10(abs(squeeze(g_totx(:, fidx, :))))), 'k-', ...
     t / 3600, std(20 * log10(abs(squeeze(gLNAx(:, fidx, :))))), 'b--', ...
     t / 3600, std(20 * log10(abs(squeeze(gFEMx(:, fidx, :))))), 'm--', ...
     t / 3600, std(20 * log10(abs(squeeze(gFOx(:, fidx, :))))), 'r--', ...
     t / 3600, std(20 * log10(abs(squeeze(g_preADUx(:, fidx, :))))), 'g--');
set(gca, 'FontSize', 16);
xlabel('time (hours)');
ylabel('RMS gain magnitude (dB)');
title(['RMS gain magnitude at ' num2str(f(fidx) / 1e6) ' MHz']);
legend('total', 'LNA', 'FEM', 'FO', 'pre-ADU', 'Location', 'SouthEast');

%% RMS gain phase variation after removing average gain per receiver
Nant = size(g_totx, 1);
Nt = length(t);
% emulate perfect calibration
cal_raw = zeros(Nant, Nt);
for tidx = 1:Nt
    cal_raw(:, tidx) = 1 ./ (g_totx(:, fidx, tidx) / g_totx(1, fidx, tidx));
end
cal = mean(cal_raw, 2);
% "calibrate" gains
g_totx_cal = diag(cal) * squeeze(g_totx(:, fidx, :));
gLNAx_cal = diag(cal) * squeeze(gLNAx(:, fidx, :));

figure
plot(t / 3600, std(20 * log10(abs(squeeze(g_totx(:, fidx, :))))), 'r-', ...
     t / 3600, std(20 * log10(abs(g_totx_cal))), 'b-');
set(gca, 'FontSize', 16);
xlabel('time (hours)');
ylabel('RMS gain magnitude (dB)');
title(['RMS gain magnitude at ' num2str(f(fidx) / 1e6) ' MHz']);
legend('before calibration', 'after calibration', 'Location', 'East');

figure
plot(t / 3600, std(unwrap(angle(squeeze(g_totx(:, fidx, :))))) * 180 / pi, 'r-', ...
     t / 3600, std(unwrap(angle(g_totx_cal), [], 2), [], 1) * 180 / pi, 'b-');
set(gca, 'FontSize', 16);
xlabel('time (hours)');
ylabel('RMS phase (deg)');
title(['RMS gain phase at ' num2str(f(fidx) / 1e6) ' MHz']);
legend('before calibration', 'after calibration', 'Location', 'East');
    
%% impact on station beam
l = -1:0.02:1;
m = -1:0.02:1;
[lgrid, mgrid] = meshgrid(l, m);
g = zeros(2 * Nant, 1);
% time of maximum RMS gain variations
[~, tidx] = max(std(squeeze(g_totx(:, fidx, :))));
% interleave gains of x- and y- receive paths
g(1:2:end) = g_totx(:, fidx, tidx);
g(2:2:end) = g_toty(:, fidx, tidx);
l0 = 0.1;
m0 = 0.3;
Jbeam = SKA_LOW_statbeam(l0, m0, f(fidx), g, lgrid(:), mgrid(:));
% unperturbed beam
Jbeam0 = SKA_LOW_statbeam(l0, m0, f(fidx), ones(2 * Nant, 1), lgrid(:), mgrid(:));

%% plot perturbed beam
figure
set(gcf, 'Position', [0 0 1120 840]);
subplot(2, 2, 1);
imagesc(l, m, abs(reshape(Jbeam(1, 1, :), [length(m), length(l)])));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('|J_{x\theta}|');

subplot(2, 2, 2);
imagesc(l, m, abs(reshape(Jbeam(1, 2, :), [length(m), length(l)])));
set(gca, 'FontSize', 16, 'YDir', 'normal');
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('|J_{x\phi}|');

subplot(2, 2, 3);
imagesc(l, m, abs(reshape(Jbeam(2, 1, :), [length(m), length(l)])));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('|J_{y\theta}|');

subplot(2, 2, 4);
imagesc(l, m, abs(reshape(Jbeam(2, 2, :), [length(m), length(l)])));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('|J_{y\phi}|');

%% plot difference of magnitude with unperturbed beam

% normalize on beam peak
[~, beamidx] = min(abs(lgrid(:) - l0) + abs(mgrid(:) - m0));
Ndir = length(lgrid(:));
Jbeam_pointing = squeeze(Jbeam(:, :, beamidx));
Jbeam0_pointing = squeeze(Jbeam0(:, :, beamidx));
for diridx = 1:Ndir
    Jbeam(:, :, diridx) = Jbeam_pointing \ squeeze(Jbeam(:, :, diridx));
    Jbeam0(:, :, diridx) = Jbeam0_pointing \ squeeze(Jbeam0(:, :, diridx));
end

figure
set(gcf, 'Position', [0 0 1120 840]);
subplot(2, 2, 1);
imagesc(l, m, reshape(abs(Jbeam(1, 1, :)) - abs(Jbeam0(1, 1, :)), [length(m), length(l)]));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('fractional difference in |J_{x\theta}|');

subplot(2, 2, 2);
imagesc(l, m, reshape(abs(Jbeam(1, 2, :)) - abs(Jbeam0(1, 2, :)), [length(m), length(l)]));
set(gca, 'FontSize', 16, 'YDir', 'normal');
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('fractional difference in |J_{x\phi}|');

subplot(2, 2, 3);
imagesc(l, m, reshape(abs(Jbeam(2, 1, :)) - abs(Jbeam0(2, 1, :)), [length(m), length(l)]));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('fractional difference in |J_{y\theta}|');

subplot(2, 2, 4);
imagesc(l, m, reshape(abs(Jbeam(2, 2, :)) - abs(Jbeam0(2, 2, :)), [length(m), length(l)]));
set(gca, 'FontSize', 16, 'YDir', 'normal')
set(colorbar, 'FontSize', 16);
xlabel('West \leftarrow l \rightarrow East');
ylabel('South \leftarrow m  \rightarrow North');
title('fractional difference in |J_{y\phi}|');

%% impact on imaging - need coordinate checking, should R be conjugate before imaging?
tobs = timestamp(1081);
load Arraylayout_DiNinni_SKA-LOW_38m.mat
lon = 116.7 * (pi / 180);               % longitude of AAVS site in rad
lat = -26.7 * (pi / 180);               % latitude of AAVS site in rad
R = generate_vis_diffuse_pol(Arraylayout, f(fidx), tobs, lon, lat, @pol_EEP_SKALA4AL);

%% unperturbed sky image in XX
l = -1:0.02:1;
m = -1:0.02:1;
figure
% note: R needs to be conjugated to match sign of delays in EEP simulations
% with sign of FFT in LOFAR filter bank
skymap0 = acm2skyimage(conj(R(1:2:end, 1:2:end)), Arraylayout(:, 1), Arraylayout(:, 2), f(fidx), l, m);
imagesc(m, l, skymap0);
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('South \leftarrow m \rightarrow North');
ylabel('East \leftarrow l \rightarrow West');
title('dirty image with gains corrected');

% perturbed sky image in XX
figure
g = g_totx(:, fidx, 50);
skymap = acm2skyimage(diag(g) * conj(R(1:2:end, 1:2:end)) * diag(g)', Arraylayout(:, 1), Arraylayout(:, 2), f(fidx), l, m);
% scale image to remove effect of gain magnitude scaling
skymap = skymap * mean(skymap0(:)) / mean(skymap(:));
imagesc(m, l, skymap);
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('South \leftarrow m \rightarrow North');
ylabel('East \leftarrow l \rightarrow West');
title('dirty image with uncorrected gains');

% difference image
figure
imagesc(m, l, (skymap - skymap0) ./ max(skymap0(:)));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('South \leftarrow m \rightarrow North');
ylabel('East \leftarrow l \rightarrow West');
title('difference image, fraction of peak flux');
