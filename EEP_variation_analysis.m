%% Simulation of EEP induced variations
%
% This script simulates the apparent receive path gain variations and
% station beam gain variations induced by ignoring, i.e., not correcting
% for, EEP differences while tracking a source at a specified position.
% This script was written to allow comparison of the receive path gain
% variations induced by variations in component temperature with those
% induced by another factor intrinsic to the observation process.
%
% Stefan J. Wijnholds, 4 May 2021
% Last modified on May 11, 2021 by Stefan J. Wijnholds

%% start with a clean workspace
clear
close all

%% define simulation
t = 0:60:24*3600;           % time since start of observation in s
rasrc = 0;                  % right ascension of source in rad
decsrc = -10*(pi/180);      % declination of source in rad
freq = 110e6;               % observing frequency in Hz
lon = 116.7 * (pi / 180);   % longitude of AAVS site in rad
lat = -26.7 * (pi / 180);   % latitude of AAVS site in rad

tstart = datenum(2021, 5, 4, 0, 0, 0);  % start time of observation
tobs = tstart + t / (24 * 3600);        % observing time as datenum

%% calculate track in (l,m) coordinates
Nt = length(tobs);
lsrc = NaN(Nt, 1);
msrc = NaN(Nt, 1);
for tidx = 1:Nt
    [lsrc(tidx), msrc(tidx)] = radectolm(rasrc, decsrc, JulianDay(tobs(tidx)), lon * (180 / pi), lat * (180 / pi));
end

% show (l, m)-track
figure
plot(lsrc, msrc, '.');
set(gca, 'FontSize', 16);
axis([-1 1 -1 1]);
axis equal
xlabel('l');
ylabel('m');

%% EEPs towards unpolarized source
J = pol_EEP_SKALA4AL(lsrc, msrc);

% calculate gain towards unpolarized source
gx = sqrt(abs(squeeze(J(1, 1, :, :))).^2 + abs(squeeze(J(1, 2, :, :))).^2);
gy = sqrt(abs(squeeze(J(2, 1, :, :))).^2 + abs(squeeze(J(2, 2, :, :))).^2);

% show gains
figure
set(gcf, 'Position', [0, 0, 1120, 420]);
subplot(1, 2, 1);
plot(t / 3600, gx.');
set(gca, 'FontSize', 16);
xlabel('time since start of observation (hours)');
ylabel('voltage gain');
title('x-polarization');

subplot(1, 2, 2);
plot(t / 3600, gy.');
set(gca, 'FontSize', 16);
xlabel('time since start of observation (hours)');
ylabel('voltage gain');
title('y-polarization');

%% station beam gain variations while tracking source
Nant = size(gx, 1);
% define grid for main beam area and first sidelobe
lm = -0.16:0.02:0.16;
[lgrid, mgrid] = meshgrid(lm);
Ndir = length(lgrid(:));
% calculate polarized station beam
Jstat = NaN(2, 2, Ndir, Nt);
for tidx = 1:Nt
    % report progress
    if mod(tidx-1, 100) == 0
        disp(['working on timeslot ' num2str(tidx) ' of ' num2str(Nt)]);
    end
    % avoid unnecessary calculations
    if isnan(lsrc(tidx))
        continue
    end
    Jstat(:, :, :, tidx) = SKA_LOW_statbeam(lsrc(tidx), msrc(tidx), freq, ones(2 * Nant, 1), lsrc(tidx) + lgrid(:), msrc(tidx) + mgrid(:));
end
% calculate station beam gain towards unpolarized source
gstatx = sqrt(abs(squeeze(Jstat(1, 1, :, :))).^2 + abs(squeeze(Jstat(1, 2, :, :))).^2);
gstaty = sqrt(abs(squeeze(Jstat(2, 1, :, :))).^2 + abs(squeeze(Jstat(2, 2, :, :))).^2);

% represent as normalized beam images
imsize = sqrt(Ndir);
beamx = zeros(imsize, imsize, Nt);
beamy = zeros(imsize, imsize, Nt);
for tidx = 1:Nt
    beamx(:, :, tidx) = reshape(gstatx(:, tidx), [imsize, imsize]);
    beamx(:, :, tidx) = beamx(:, :, tidx) / beamx(ceil(imsize/2), ceil(imsize/2), tidx);
    beamy(:, :, tidx) = reshape(gstaty(:, tidx), [imsize, imsize]);
    beamy(:, :, tidx) = beamy(:, :, tidx) / beamy(ceil(imsize/2), ceil(imsize/2), tidx);
end
% check for ill-defined data
sel = ~isnan(squeeze(sum(sum(beamx, 1), 2)));

%% show average station beam during observation
figure
set(gcf, 'Position', [0, 0, 1120, 420]);
subplot(1, 2, 1);
imagesc(lm, lm, mean(beamx(:, :, sel), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average station beam, x-pol');

subplot(1, 2, 2);
imagesc(lm, lm, mean(beamy(:, :, sel), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average station beam, y-pol');

% show standard deviation of beam variations during observation
figure
set(gcf, 'Position', [0, 0, 1120, 420]);
subplot(1, 2, 1);
imagesc(lm, lm, std(beamx(:, :, sel), 0, 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('std of beam varations, x-pol');

subplot(1, 2, 2);
imagesc(lm, lm, std(beamy(:, :, sel), 0, 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('std of beam varations, y-pol');

%% compare with predicted beam based on array factor and average EEP
load Arraylayout_DiNinni_SKA-LOW_38m.mat Arraylayout
xpos = Arraylayout(:, 1);
ypos = Arraylayout(:, 2);
AF = zeros(imsize, imsize, Nt);
beammodelx = zeros(imsize, imsize, Nt);
beammodely = zeros(imsize, imsize, Nt);
% calculate average EEP
lmEEP = -1:0.02:1;
EEPsize = length(lmEEP);
[lEEP, mEEP] = meshgrid(lmEEP);
J_EEP = pol_EEP_SKALA4AL(lEEP(:), mEEP(:));
gEEPx = mean(sqrt(abs(squeeze(J_EEP(1, 1, :, :))).^2 + abs(squeeze(J_EEP(1, 2, :, :))).^2));
gEEPy = mean(sqrt(abs(squeeze(J_EEP(2, 1, :, :))).^2 + abs(squeeze(J_EEP(2, 2, :, :))).^2));
for tidx = 1:Nt
    % report progress
    if mod(tidx-1, 100) == 0
        disp(['working on timeslot ' num2str(tidx) ' of ' num2str(Nt)]);
    end
    % avoid unnecessary calculations
    if isnan(lsrc(tidx))
        continue
    end
    % calculate AF
    AF(:, :, tidx) = calculate_AF(xpos, ypos, eye(length(xpos)), lsrc(tidx) + lm, msrc(tidx) + lm, freq, lsrc(tidx), msrc(tidx));
    % interpolate average EEP
    gx = interp2(lmEEP, lmEEP, reshape(gEEPx, [EEPsize, EEPsize]), lsrc(tidx) + lgrid(:), msrc(tidx) + mgrid(:));
    gy = interp2(lmEEP, lmEEP, reshape(gEEPy, [EEPsize, EEPsize]), lsrc(tidx) + lgrid(:), msrc(tidx) + mgrid(:));
    % calculate and normalize beam model
    beammodelx(:, :, tidx) = reshape(gx, [imsize, imsize]) .* AF(:, :, tidx);
    beammodelx(:, :, tidx) = beammodelx(:, :, tidx) / beammodelx(ceil(imsize/2), ceil(imsize/2), tidx);
    beammodely(:, :, tidx) = reshape(gy, [imsize, imsize]) .* AF(:, :, tidx);
    beammodely(:, :, tidx) = beammodely(:, :, tidx) / beammodely(ceil(imsize/2), ceil(imsize/2), tidx);
end

% calculate difference between predicted beam and
% 1. model based on array factor
% 2. model based on array factor and average EEP
diff_AF_x = NaN(imsize, imsize, Nt);
diff_AF_y = NaN(imsize, imsize, Nt);
diff_model_x = NaN(imsize, imsize, Nt);
diff_model_y = NaN(imsize, imsize, Nt);
for tidx = 1:Nt
    diff_AF_x(:, :, tidx) = beamx(:, :, tidx) - abs(AF(:, :, tidx) / AF(ceil(imsize/2), ceil(imsize/2), tidx));
    diff_model_x(:, :, tidx) = beamx(:, :, tidx) - abs(beammodelx(:, :, tidx));
    
    diff_AF_y(:, :, tidx) = beamy(:, :, tidx) - abs(AF(:, :, tidx) / AF(ceil(imsize/2), ceil(imsize/2), tidx));
    diff_model_y(:, :, tidx) = beamy(:, :, tidx) - abs(beammodely(:, :, tidx));
end
% check for ill-defined data
sel = ~isnan(squeeze(sum(sum(diff_model_x, 1), 2)));

figure
set(gcf, 'Position', [0, 0, 1120, 840]);
subplot(2, 2, 1);
imagesc(lm, lm, mean(abs(diff_AF_x(:, :, sel)), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average absolute difference with AF, x-pol');

subplot(2, 2, 2);
imagesc(lm, lm, mean(abs(diff_model_x(:, :, sel)), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average absolute difference with model, x-pol');

subplot(2, 2, 3);
imagesc(lm, lm, mean(abs(diff_AF_y(:, :, sel)), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average absolute difference with AF, y-pol');

subplot(2, 2, 4);
imagesc(lm, lm, mean(abs(diff_model_y(:, :, sel)), 3));
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('\Delta m');
ylabel('\Delta l');
title('average absolute difference with model, y-pol');
