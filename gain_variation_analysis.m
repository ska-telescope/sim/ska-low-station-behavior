%% Receive path gain variation analysis
%
% This script performs an analysis of the impact of the gain variations
% predicted by the receive path gain model defined in the
% receive_path_gain.m script. To this end, the receive paths will be
% "calibrated" assuming some error and noise free calibration mechanism
% before the residual gain variations are assessed.
%
% Stefan J. Wijnholds, 9 February 2021
% Last modified by Stefan J. Wijnholds on 26 April 2021

%% Initialise workspace
close all
cleanup = true;
if cleanup
    % clean up workspace and run the receive path gain model
    clear
    receive_path_gain
end
% if no clean up is needed, it is assumed that the receive_path_gain.m
% script has been run just prior to this script

%% Calibrate the data

% use the gains of the first time instance as calibration solutions
calx = 1 ./ g_totx(:, :, 1);
caly = 1 ./ g_toty(:, :, 1);
% apply calibration factors
g_totx_cal = zeros(size(g_totx));
g_toty_cal = zeros(size(g_toty));
for tidx = 1:Nt
    g_totx_cal(:, :, tidx) = calx .* g_totx(:, :, tidx);
    g_toty_cal(:, :, tidx) = caly .* g_toty(:, :, tidx);
end

%% Show gain variations
tstep = 15;     % step size while going through time samples
figure
set(gcf, 'Position', [0 0 1680 840]);
for tidx = 1:tstep:Nt
    subplot(2, 3, 1);
    plot((t - t(1)) / 3600, Tamb);
    line((t(tidx) - t(1)) / 3600 * [1; 1], [20 40], 'Color', [1 0 0]);
    set(gca, 'FontSize', 16);
    xlabel(['time in hours since ' datestr(timestamp(1))]);
    ylabel('ambient temperature (degrees C)');
    title(datestr(timestamp(tidx)));

    subplot(2, 3, 4);
    plot((t - t(1)) / 3600, TFEM - 273);
    line((t(tidx) - t(1)) / 3600 * [1; 1], [20 70], 'Color', [1 0 0]);
    set(gca, 'FontSize', 16);
    xlabel(['time in hours since ' datestr(timestamp(1))]);
    ylabel('FEM temperature (degrees C)');

    subplot(2, 3, 2);
    plot(f / 1e6, abs(g_totx_cal(:, :, tidx)).');
    set(gca, 'FontSize', 16);
    axis([50 350 0.7 1.2]);
    xlabel('frequency (MHz)');
    ylabel('residual gain, x-pol');

    subplot(2, 3, 3);
    plot(f / 1e6, angle(g_totx_cal(:, :, tidx)).' * 180 / pi);
    set(gca, 'FontSize', 16);
    axis([50 350 -200 200]);
    xlabel('frequency (MHz)');
    ylabel('residual phase, x-pol (degrees)');

    subplot(2, 3, 5);
    plot(f / 1e6, abs(g_toty_cal(:, :, tidx)).');
    set(gca, 'FontSize', 16);
    axis([50 350 0.7 1.2]);
    xlabel('frequency (MHz)');
    ylabel('residual gain, y-pol');
    
    subplot(2, 3, 6);
    plot(f / 1e6, angle(g_toty_cal(:, :, tidx)).' * 180 / pi);
    set(gca, 'FontSize', 16);
    axis([50 350 -200 200]);
    xlabel('frequency (MHz)');
    ylabel('residual phase, y-pol (degrees)');
    pause(0.1);
end

%% Statistical analysis of gain variations
std_gx_mag = zeros(Nf, Nt);
std_gy_mag = zeros(Nf, Nt);
std_gx_ph = zeros(Nf, Nt);
std_gy_ph = zeros(Nf, Nt);
for tidx = 1:Nt
    std_gx_mag(:, tidx) = std(abs(g_totx_cal(:, :, tidx)));
    std_gx_ph(:, tidx) = std(unwrap(angle(g_totx_cal(:, :, tidx)).') * 180 / pi, [], 2);
    std_gy_mag(:, tidx) = std(abs(g_toty_cal(:, :, tidx)));
    std_gy_ph(:, tidx) = std(unwrap(angle(g_toty_cal(:, :, tidx)).') * 180 / pi, [], 2);
end
% show result
figure
set(gcf, 'Position', [0 0 1120 840]);
subplot(2, 2, 1);
pcolor((t - t(1)) / 3600, f / 1e6, std_gx_mag);
shading flat
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('time (hours)');
ylabel('frequency (MHz)');
title('std(|g_x|)');

subplot(2, 2, 2);
pcolor((t - t(1)) / 3600, f / 1e6, std_gx_ph);
shading flat
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('time (hours)');
ylabel('frequency (MHz)');
title('std(arg(g_x)) in degrees');

set(gcf, 'Position', [0 0 1120 840]);
subplot(2, 2, 3);
pcolor((t - t(1)) / 3600, f / 1e6, std_gy_mag);
shading flat
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('time (hours)');
ylabel('frequency (MHz)');
title('std(|g_y|)');

subplot(2, 2, 4);
pcolor((t - t(1)) / 3600, f / 1e6, std_gy_ph);
shading flat
set(gca, 'FontSize', 16);
set(colorbar, 'FontSize', 16);
xlabel('time (hours)');
ylabel('frequency (MHz)');
title('std(arg(g_y)) in degrees');

%% show impact on station beam
tstep = 60;         % step size while going through time samples
tstart = 12 * 60 + 1;   % only go through daytime samples
tstop = 24 * 60 + 1;
l0 = 0.1;       % point beam off-center to avoid EEP interpolation issues
m0 = 0.3;
fidx = find(f == 110e6);
lm = -1:0.02:1;
[lgrid, mgrid] = meshgrid(lm);
gcal = zeros(2 * Nant, Nt);
gcal(1:2:end, :) = g_totx_cal(:, fidx, :);
gcal(2:2:end, :) = g_toty_cal(:, fidx, :);
g0 = ones(2 * Nant);
Nsnapshot = floor((tstop - tstart) / tstep) + 1;
% station beam after calibration
Jbeam = zeros(2, 2, length(lgrid(:)), Nsnapshot);
% array factor after calibration
AFx = zeros(length(lm), length(lm), Nsnapshot);
AFy = zeros(length(lm), length(lm), Nsnapshot);
% unperturbed station beam
Jbeam0 = SKA_LOW_statbeam(l0, m0, f(fidx), g0, lgrid(:), mgrid(:));
% unperturbed array factor
load Arraylayout_DiNinni_SKA-LOW_38m.mat Arraylayout
xpos = Arraylayout(:, 1);
ypos = Arraylayout(:, 2);
AF0 = calculate_AF(xpos, ypos, eye(length(xpos)), lm, lm, f(fidx), l0, m0);
% normalize on beam peak
[~, beamidx] = min(abs(lgrid(:) - l0) + abs(mgrid(:) - m0));
Ndir = length(lgrid(:));
Jbeam0_pointing = squeeze(Jbeam0(:, :, beamidx));
for diridx = 1:Ndir
    Jbeam0(:, :, diridx) = Jbeam0_pointing \ squeeze(Jbeam0(:, :, diridx));
end
maxval = max(abs(AF0(:)));
AF0 = AF0 / maxval;
figure
set(gcf, 'Position', [0 0 1890 1050]);
for tidx = tstart:tstep:tstop
    snapshot_idx = (tidx - tstart) / tstep + 1;
    disp(['calculating beams for timeslot ' num2str(snapshot_idx) ' of ' num2str(Nsnapshot)]);
    Jbeam(:, :, :, snapshot_idx) = SKA_LOW_statbeam(l0, m0, f(fidx), gcal(:, tidx), lgrid(:), mgrid(:));
    % normalize on beam peak
    Jbeam_pointing = squeeze(Jbeam(:, :, beamidx, snapshot_idx));
    for diridx = 1:Ndir
        Jbeam(:, :, diridx, snapshot_idx) = Jbeam_pointing \ squeeze(Jbeam(:, :, diridx, snapshot_idx));
    end
    % normalised array factor
    AFx(:, :, snapshot_idx) = calculate_AF(xpos, ypos, diag(gcal(1:2:end, tidx)), lm, lm, f(fidx), l0, m0);
    maxvalx = max(max(AFx(:, :, snapshot_idx)));
    AFx(:, :, snapshot_idx) = AFx(:, :, snapshot_idx) / maxvalx;
    AFy(:, :, snapshot_idx) = calculate_AF(xpos, ypos, diag(gcal(2:2:end, tidx)), lm, lm, f(fidx), l0, m0);
    maxvaly = max(max(AFy(:, :, snapshot_idx)));
    AFy(:, :, snapshot_idx) = AFy(:, :, snapshot_idx) / maxvaly;
    % show perturbed, full-polarized beam
    subplot(3, 4, 1);
    imagesc(lm, lm, abs(reshape(Jbeam(1, 1, :, snapshot_idx), [length(lm), length(lm)])));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title(['|J_{x\theta}|, ', datestr(timestamp(tidx))]);

    subplot(3, 4, 2);
    imagesc(lm, lm, abs(reshape(Jbeam(1, 2, :, snapshot_idx), [length(lm), length(lm)])));
    set(gca, 'FontSize', 16, 'YDir', 'normal');
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('|J_{x\phi}|');

    subplot(3, 4, 3);
    imagesc(lm, lm, abs(reshape(Jbeam(2, 1, :, snapshot_idx), [length(lm), length(lm)])));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('|J_{y\theta}|');

    subplot(3, 4, 4);
    imagesc(lm, lm, abs(reshape(Jbeam(2, 2, :, snapshot_idx), [length(lm), length(lm)])));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('|J_{y\phi}|');
    
    % show fractional difference with unperturbed beam
    subplot(3, 4, 5);
    imagesc(lm, lm, reshape(abs(Jbeam(1, 1, :, snapshot_idx)) - abs(Jbeam0(1, 1, :)), [length(lm), length(lm)]));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference in |J_{x\theta}|');

    subplot(3, 4, 6);
    imagesc(lm, lm, reshape(abs(Jbeam(1, 2, :, snapshot_idx)) - abs(Jbeam0(1, 2, :)), [length(lm), length(lm)]));
    set(gca, 'FontSize', 16, 'YDir', 'normal');
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference in |J_{x\phi}|');

    subplot(3, 4, 7);
    imagesc(lm, lm, reshape(abs(Jbeam(2, 1, :, snapshot_idx)) - abs(Jbeam0(2, 1, :)), [length(lm), length(lm)]));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference in |J_{y\theta}|');

    subplot(3, 4, 8);
    imagesc(lm, lm, reshape(abs(Jbeam(2, 2, :, snapshot_idx)) - abs(Jbeam0(2, 2, :)), [length(lm), length(lm)]));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference in |J_{y\phi}|');

    % show result from array factor analysis
    subplot(3, 4, 9);
    imagesc(lm, lm, abs(AFx(:, :, snapshot_idx)))
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('AF, x-pol');
    
    subplot(3, 4, 10);
    imagesc(lm, lm, abs(AFx(:, :, snapshot_idx) - AF0));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference AF, x-pol');

    subplot(3, 4, 11);
    imagesc(lm, lm, abs(AFx(:, :, snapshot_idx)))
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('AF, y-pol');
    
    subplot(3, 4, 12);
    imagesc(lm, lm, abs(AFx(:, :, snapshot_idx) - AF0));
    set(gca, 'FontSize', 16, 'YDir', 'normal')
    set(colorbar, 'FontSize', 16);
    xlabel('West \leftarrow l \rightarrow East');
    ylabel('South \leftarrow m  \rightarrow North');
    title('fractional difference AF, y-pol');

    pause(0.1);
end
