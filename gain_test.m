% This script was written for DP ART System Demo 8.5
%
% This script aims to test whether the gains provided to OSKAR are correctly
% applied and can be retrieved by station calibration assuming that a good
% source model is available. It is assumed that OSKAR has simulated two
% sets of visibilities: one with gain corruptions included (the "data") and
% one without gain corruptions (the "model").
%
% SJW, 29 October 2020
% last updated by Stefan J. Wijnholds on 10 November 2020

%% start with a clean workspace
clear
close all

%% input data and meta-data

% directory in which inputndata resides
datadir = '/Users/wijnholds/SKA/SKA-low bridging/SKA-low calibration/OSKAR/';

posfile = 'single_station.tm/layout.txt';   % antenna positions
visdata = 'vis_with_gain_errors.mat';       % converted visibility data
vismodel = 'vis_model.mat';                 % converted visibility model
gfile = 'single_station.tm/gain_model.h5';  % true gains
freq = 100e6;                               % simulation frequency (Hz)
lm = -1:0.02:1;                             % lm-grid for imaging
Nt = 100;                                   % number of time slices
tol = 1e-6;                                 % tolerance used in StEFCal
DoImaging = false;                          % indicate whether images are compared

%% read antenna positions
fid = fopen([datadir posfile], 'r');
pos = fscanf(fid, '%f,%f');
xpos = pos(1:2:end);
ypos = pos(2:2:end);
Nant = length(xpos);
fclose(fid);

%% read visibility data and visibility model

% visibility data
load([datadir visdata]);
sel = triu(ones(Nant), 1);
% read xx-correlations
Rxx = zeros(Nant, Nant, Nt);
Nvis = Nant * (Nant - 1) / 2;
for tidx = 1:Nt
    R = zeros(Nant);
    R(sel.' == 1) = vis(((tidx - 1) * Nvis + 1):(tidx * Nvis), 1, 1);
    Rxx(:, :, tidx) = R + R';
end

% visibility model
load([datadir vismodel]);
sel = triu(ones(Nant), 1);
% read xx-correlations
Rxx0 = zeros(Nant, Nant, Nt);
for tidx = 1:Nt
    R = zeros(Nant);
    R(sel.' == 1) = vis(((tidx - 1) * Nvis + 1):(tidx * Nvis), 1, 1);
    Rxx0(:, :, tidx) = R + R';
end

% compare images with and without gain perturbations if desired
if DoImaging
    Ixx = acm2skyimage(Rxx, xpos, ypos, freq * ones(Nt, 1), lm, lm);
    Ixx0 = acm2skyimage(Rxx0, xpos, ypos, freq * ones(Nt, 1), lm, lm);
    % define mask to remove unphysical area
    lmdist = sqrt(meshgrid(lm).^2 + meshgrid(lm).'.^2);
    mask = ones(length(lm));
    mask(lmdist > 1) = NaN;
    % show result
    figure;
    for tidx = 1:Nt
        subplot(1, 2, 1);
        imagesc(Ixx(:, :, tidx) .* mask);
        colorbar
        title(['data, snapshot nr. ', num2str(tidx)]);
        subplot(1, 2, 2);
        imagesc(Ixx0(:, :, tidx) .* mask);
        colorbar
        title(['model, snapshot nr. ', num2str(tidx)]);
        pause
    end
end

%% Calibrate using StEFCal

% estimate gains
gest = zeros(Nant, Nt);
for tidx = 1:Nt
    gest(:, tidx) = gainsolv(tol, Rxx0(:, :, tidx), Rxx(:, :, tidx), ones(Nant, 1));
    % use first element as phase reference
    gest(:, tidx) = gest(:, tidx) / (gest(1, tidx) / abs(gest(1, tidx)));
end
%% read true gain values
gfreq = h5read([datadir gfile], '/freq (Hz)');
fidx = find(gfreq == freq);
gxdata = h5read([datadir gfile], '/gain_xpol');
gx = gxdata.r + 1i * gxdata.i;
gx(isnan(gx)) = 0;
% use first antenna as gain reference
for tidx = 1:Nt
    gnorm = abs(gx(1, :, tidx)) ./ gx(1, :, tidx);
    gnorm(isnan(gnorm)) = 1;
    gx(:, :, tidx) = gx(:, :, tidx) * diag(gnorm);
end

%% plot relative gain amplitude errors
figure
plot((abs(squeeze(gx(:, fidx, :))).' - abs(gest).') ./ abs(squeeze(gx(:, fidx, :))).');
set(gca, 'FontSize', 16);
xlabel('snapshot index');
ylabel('relative gain amplitude error');

%% plot gain phase errors
figure
plot(angle(squeeze(gx(:, fidx, :))).' - angle(gest).');
set(gca, 'FontSize', 16);
xlabel('snapshot index');
ylabel('gain phase errors (rad)');
